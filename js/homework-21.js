"use strict"

// Второе решение 3-го задания ДЗ-20:

// function getNameOfMonth(n) {
//     if (n === 0) return 'Январь';
//     if (n === 1) return 'Февраль';
//     if (n === 2) return 'Март';
//     if (n === 3) return 'Апрель';
//     if (n === 4) return 'Май';
//     if (n === 5) return 'Июнь';
//     if (n === 6) return 'Июль';
//     if (n === 7) return 'Август';
//     if (n === 8) return 'Сентябрь';
//     if (n === 9) return 'Октябрь';
//     if (n === 10) return 'Ноябрь';
//     if (n === 11) return 'Декабрь';
// }

// console.log(getNameOfMonth(0));

// for (let n = 0; n < 12; n++) {
//     if (n !== 9) {
//        console.log(getNameOfMonth(n));
//     }
//  }

// Упражнение 1
// Напишите функцию isEmpty(obj), которая возвращает true,
// если у объекта нет свойств, иначе false. Должна корректно
// работать абсолютно для любого объекта. Добавьте для данной
// функции комментарий в виде JSDoc с описанием того, что она
// делает, какие параметры принимает и что возвращает.

/**
 * Циклом перебираем свойства объекта:
 * obj = {}; - возвращаем true;
 * obj.heigt = 18; - возвращаем false.
 */

let obj = {};

// obj.heigt = 18;

function isEmpty(obj) {
    for (let key in obj) {
        return false;
    }

    return true;
}

console.log(isEmpty(obj));

// Упражнение 2 - смотри data.js

// Упражнение 3

let salaries = {
    John: 100000,
    Ann: 160000,
    Pete: 130000,
};

/**
 * Повышает зарплату сотрудникам
 * @param {number} perzent - Процент, на который нужно повысить зарплаты сотрудников
 * @returns - Возвращает зарплату сотрудников с учетом повышения
 */

function raiseSalary(perzent) {
     let newSalaries = {};

     for (let key in salaries) {
       newSalaries[key] = salaries[key] + ((salaries[key] * perzent) / 100);
     }

    return newSalaries;
}

console.log(raiseSalary(5));

/**
 * Суммирует зарплаты после повышения
 * @param {number} salaries - Зарплата сотрудников
 * @returns - Возвращает суммарное значение всех зарплат после повышения.
 */

function totalSalary(salaries) {
    let total = 0;

    for (let key in salaries) {
      total += salaries[key];
    }

   return total;
}

console.log(totalSalary(raiseSalary(5)));